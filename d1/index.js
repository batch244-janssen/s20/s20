console.log("Hello World!")

// SECTION - "while" loop
	// If the condition is true, the statement inside the code block will be executed.
	// "Iteration" is the term given to the repetition of statements.
	/*
		Syntax:

		while (expression/condition) {
			statement
		}
	*/

	let count = 0;

	// while the value of count is not equal to 0
	while (count !== 0) {

		// The current value of count is printed out.
		console.log("While: " + count);

		// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
		count--;
	}


// SECTION "do-while" loop
	// Do-While loop worls a lot like the while loop, but unlike the while loop, do-while loops guarantee that the code will be executed at least once.

	/*
		do {
			statement
		} while (expression/condition)
	*/
				// Number() converts the received input into a number data type
	let number = Number(prompt("Give me a number"));

	do {
		console.log("Do While: " + number);

		// Increases the value of number by 1 every after iteration to stop the loop when it reaches 10 or greater.
		number += 1;

		// Providing a number of 10 or greater will run the code block once and will stop the loop
	} while (number < 10);


// SECTION - "for" loop

	/*
		for (initialization; expression/condition; finalExpression){
			statement
		}
	*/

	/*
		- Will create a loop that will start from 0 and end at 20
		- Every iteration of the loop, the value of count will be checked if it is equal or less than 20
		- If the value of count is less than or equal to 20 the statement inside of the loop will execute
		- The value of count will be incremented by one for each iteration
	*/

	// for (let count = 0; count <= 20; count++){
	// 	console.log(count);
	// }

	let myString = "alex";
	// .length shows the number of characters/elements
	console.log(myString.length);
	console.log(myString[0]);
	console.log(myString[1]);
	console.log(myString[2]);
	console.log(myString[3]);

	for(let x = 0; x < myString.length; x++){
		console.log(myString[x])
	}

	let myName = "DAISY";

	for (let i = 0; i < myName.length; i++){
		if (
			myName[i].toLowerCase() == "a" || 
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "o" ||
			myName[i].toLowerCase() == "u"
		) {
			console.log(3);
		} else {
			console.log(myName[i]);
		}
	}


// SECTION - Continue and Break statements

	for (let count = 0; count <=20; count++){

		// if remainder is equal to 0
		if (count % 2 === 0){
			// tells the code to continue to the next iteration of the loop
			// This ignores all the statements located after the continue statement.
			continue;
		}
		// The current value of number is printed out if the remainder is not equal to 0
		console.log("Continue and Break: " + count);

		// If the current value of count is greater than 10
		if (count > 10) {
			// Tells the code to terminate/stop the loop.
			// number after 11 will no longer be printed
			break;
		}
	}

	/*
		- Creates a loop that will iterate based on the length of the string
		- How this For Loop works:
		    1. The loop will start at 0 for the the value of "i"
		    2. It will check if "i" is less than the length of name (e.g. 0)
		    3. The if statement will check if the value of name[i] converted to a lowercase letter a (e.g. name[0] = a)
		    4. If the expression/condition of the if statement is true the loop will continue to the next iteration.
		    5. If the value of name[i] is not equal to a, the second if statement will be evaluated
		    6. The second if statement will check if the value of name[i] converted to a lowercase letter d (e.g. name[0] = d)
		    7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
		    8. The value of "i" will be incremented by 1 (e.g. i = 1)
		    9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] = d) is true, the loop will stop due to the "break" statement
	*/
	let name = "Taylor"
	for (let i = 0; i < name.length; i++) {

		console.log(name[i]);

		// If the vowel is equal to a, continue to the next iteration of the loop
		if (name[i].toLowerCase() === "a") {
			console.log("Continue to the next iteration");
			continue;
		}

		// If the current letter is equal to "l", stop the loop
		if (name[i] == "l") {
			break;
		}
	}


